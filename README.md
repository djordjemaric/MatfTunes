# MatfTunes :musical_note:

MatfTunes predstavlja audio plejer sa svojom bibliotekom pesama. Pesme se prvo učitavaju u biblioteku pesama, odakle je nadalje moguće puštati pesme na različite načine, menjati svaku pesmu i njene informacije, lajkovati je kao i prikazati najčešće puštane pesme. Implementrirana je funkcionalnost kreiranja i personalizacije plejlisti kao i izlistavanja i puštanja lajkovanih pesama. Takođe je moguće pretražiti pesme odnosno plejliste, i dodati tekst svake pesme pa potom pretraživati pesme po njemu. 

# Korišćene biblioteke :books:
* Qt >= 6.6
* Qt Multimedia
* TagLib >= 1.11 

# Instalacija potrebnih biblioteka :hammer:
## Qt i Qt Creator
* Qt je moguće instalirati pomoću [Qt Online Installer](https://doc.qt.io/qt-6/get-and-install-qt.html)
* Neophodno je instalirati 
    - Qt 6.6
    - Qt Multimedia (deo Additional Libraries)
    - Qt Creator (deo Development and Designer Tools)
    - CMake (deo Development and Designer Tools)

## Ostalo
* `$ sudo apt-get install libxkbcommon-dev`
* `$ sudo apt install libtag1-dev`

# Kloniranje i pokretanje :wrench:
1. Kloniranje repozitorijuma: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/MatfTunes.git`
2. Otvaranje projekta u QtCreator-u
3. Pokretanje na dugme `RUN` u donjem levom uglu ili pritiskom `CTRL + R`. 

# Demo snimak :movie_camera:
https://youtu.be/I0__d8GA79M

# Developers :computer:

 - <a href="https://gitlab.com/mcucii">Maša Cucić 34/2020</a>
 - <a href="https://gitlab.com/batpot">Marija Grujičić 35/2017</a>
 - <a href="https://gitlab.com/djordjemaric">Đorđe Marić 271/2020</a>
 - <a href="https://gitlab.com/cvija25">Lazar Cvijić 200/2020</a>
 - <a href="https://gitlab.com/bandric0">Boško Andrić 26/2020</a>
