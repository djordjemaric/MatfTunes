#ifndef JSONSERIALIZER_H
#define JSONSERIALIZER_H

#include "serializer.h"

class JsonSerializer : public Serializer
{
public:
    JsonSerializer();

    // Serializer interface
    void save(Serializable &serializable, const QString &path) final;
    void load(Serializable &serializable, const QString &path) final;

    friend class Settings;
private:
    static QString m_dir;
};

#endif // JSONSERIALIZER_H
