#include "jsonserializer.h"

#include <QFile>
#include <QJsonDocument>

QString JsonSerializer::m_dir = "";

JsonSerializer::JsonSerializer() {

}

void JsonSerializer::save(Serializable &serializable, const QString &path) {
    QJsonDocument doc = QJsonDocument::fromVariant(serializable.toVariant());
    auto filePath = m_dir;
    filePath.append(path);
    QFile file(filePath);
    file.open(QFile::WriteOnly | QFile::OpenModeFlag::Truncate);
    if (file.isOpen()) {
        file.write(doc.toJson(QJsonDocument::JsonFormat::Indented));
        file.close();
    } else {
        qDebug() << "File " + filePath + " is not open. Data has not been saved to disk.";
    }
}

void JsonSerializer::load(Serializable &serializable, const QString &path) {
    auto filePath = m_dir;
    filePath.append(path);
    QFile file(filePath);
    file.open(QFile::ReadOnly);
    if (file.isOpen()) {
        QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
        file.close();
        serializable.fromVariant(doc.toVariant());
    } else {
        qDebug() << "File " + filePath + " is not open. Data has not been loaded from disk.";
    }
}
