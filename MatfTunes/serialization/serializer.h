#ifndef SERIALIZER_H
#define SERIALIZER_H

#include "serializable.h"

#include <QString>

class Serializer
{
public:
    virtual ~Serializer() = default;
    virtual void save(Serializable &serializable, const QString &path) = 0;
    virtual void load(Serializable &serializable, const QString &path) = 0;


};

#endif // SERIALIZER_H
