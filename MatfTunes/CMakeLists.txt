cmake_minimum_required(VERSION 3.5)

project(MatfTunes VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Multimedia)

set(SERIALIZATION
            serialization/serializable.h
            serialization/serializer.h
            serialization/jsonserializer.h serialization/jsonserializer.cpp
)
set(HEADERS
        src/mainwindow.h
        src/audioplayer.h
        src/song.h
        src/songlibrary.h
        src/playlist.h
        src/playlistlibrary.h
        src/songitemui.h
        src/settings.h
        src/playlistitemui.h
        src/playeritemui.h
        src/songlibrarywindow.h
        src/editplaylistui.h
        src/editsongui.h
        src/playlistcontroller.h
        src/logslider.h
        src/implicitplaylist.h
        src/lyricssearchthread.h
        src/threadhandler.h
)
set(SOURCES
        src/main.cpp
        src/mainwindow.cpp
        src/audioplayer.cpp
        src/song.cpp
        src/songlibrary.cpp
        src/playlist.cpp
        src/playlistlibrary.cpp
        src/songitemui.cpp
        src/settings.cpp
        src/playlistitemui.cpp
        src/playeritemui.cpp
        src/songlibrarywindow.cpp
        src/editplaylistui.cpp
        src/editsongui.cpp
        src/playlistcontroller.cpp
        src/logslider.cpp
        src/implicitplaylist.cpp
        src/lyricssearchthread.cpp
        src/threadhandler.cpp
)
set(FORMS
        src/mainwindow.ui
        src/songitemui.ui
        src/playlistitemui.ui
        src/playeritemui.ui
        src/songlibrarywindow.ui
        src/editplaylistui.ui
        src/editsongui.ui
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(MatfTunes
        MANUAL_FINALIZATION
        resources.qrc
        ${SERIALIZATION}
        ${HEADERS}
        ${SOURCES}
        ${FORMS}
    )
endif()

target_link_libraries(MatfTunes PRIVATE Qt${QT_VERSION_MAJOR}::Widgets Qt${QT_VERSION_MAJOR}::Multimedia -ltag)

add_subdirectory(tests)

include(GNUInstallDirs)
install(TARGETS MatfTunes
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(MatfTunes)
endif()
