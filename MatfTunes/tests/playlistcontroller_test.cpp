#include "catch.hpp"
#include "../src/playlistcontroller.h"
#include "../src/playlist.h"
#include "../src/song.h"
#include "../src/audioplayer.h"

TEST_CASE("Testing PlaylistController class", "[class]") {
    SECTION("Testing set") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        PlaylistController::controller().setPlaylist(p);
        auto expected = p;

        //Act
        auto result = PlaylistController::controller().playlist();

        //Assert
        REQUIRE(expected == result);
        delete p;
    }

    SECTION("Testing unset") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        PlaylistController::controller().setPlaylist(p);
        PlaylistController::controller().unsetPlaylist();
        auto expected = nullptr;

        //Act
        auto result = PlaylistController::controller().playlist();

        //Assert
        REQUIRE(expected == result);

        delete p;
    }

    SECTION("Testing playing empty playlist") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        PlaylistController::controller().setPlaylist(p);
        PlaylistController::controller().playPlaylist();

        //Act+Assert
        REQUIRE_FALSE(AudioPlayer::player().playing());

        PlaylistController::controller().unsetPlaylist();
        delete p;
    }

    SECTION("Testing playing non-empty playlist") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        Song* s = new Song("1.mp3");
        p->addSong(s);
        PlaylistController::controller().setPlaylist(p);
        PlaylistController::controller().playPlaylist();
        auto expected2 = s;

        //Act
        auto result1 = AudioPlayer::player().playing();
        auto result2 = p->currentSong();

        //Assert
        REQUIRE(result1);
        REQUIRE(expected2 == result2);

        AudioPlayer::player().stopPlaying();
        PlaylistController::controller().unsetPlaylist();
        delete p;
        delete s;
    }

    SECTION("Testing playNext") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        p->addSong(s1);
        p->addSong(s2);
        PlaylistController::controller().setPlaylist(p);
        PlaylistController::controller().playPlaylist();
        auto expected1 = s1;
        auto expected2 = s2;

        //Act
        auto result1 = p->currentSong();

        PlaylistController::controller().nextSong();
        auto result2 = p->currentSong();

        //Assert
        REQUIRE(expected1 == result1);
        REQUIRE(expected2 == result2);

        AudioPlayer::player().stopPlaying();
        PlaylistController::controller().unsetPlaylist();
        delete p;
        delete s1;
        delete s2;

    }

    SECTION("Testing playPrevious") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        p->addSong(s1);
        p->addSong(s2);

        PlaylistController::controller().setPlaylist(p);
        PlaylistController::controller().playPlaylist();
        auto expected1 = s1;
        auto expected3 = s1;
        auto expected2 = s2;

        //Act
        auto result1 = p->currentSong();

        PlaylistController::controller().nextSong();

        auto result2 = p->currentSong();

        PlaylistController::controller().previousSong();

        auto result3 = p->currentSong();

        //Assert
        REQUIRE(expected1 == result1);
        REQUIRE(expected2 == result2);
        REQUIRE(expected3 == result3);

        AudioPlayer::player().stopPlaying();
        PlaylistController::controller().unsetPlaylist();
        delete p;
        delete s1;
        delete s2;
    }

    SECTION("Testing shuffle") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        p->addSong(s1);
        p->addSong(s2);
        PlaylistController::controller().setPlaylist(p);
        PlaylistController::controller().playPlaylist();
        auto expected1 = s1;
        auto expected4 = s1;

        //Act
        auto result1 = p->currentSong();

        auto result2 = PlaylistController::controller().shuffle();

        PlaylistController::controller().toggleShuffle();

        auto result3 = PlaylistController::controller().shuffle();

        PlaylistController::controller().nextSong();

        auto result4 = p->currentSong();

        //Assert
        REQUIRE(expected1 == result1);
        REQUIRE_FALSE(result2);
        REQUIRE(result3);
        REQUIRE_FALSE(expected4 == result4);

        AudioPlayer::player().stopPlaying();
        PlaylistController::controller().unsetPlaylist();
        delete p;
        delete s1;
        delete s2;
    }

    SECTION("Testing playing at correct index") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        p->addSong(s1);
        p->addSong(s2);
        PlaylistController::controller().setPlaylist(p);
        auto expected1 = s1;

        //Act
        REQUIRE_NOTHROW(PlaylistController::controller().playAtIndex(0));
        auto result1 = p->currentSong();

        //Assert
        REQUIRE(expected1 == result1);

        AudioPlayer::player().stopPlaying();
        PlaylistController::controller().unsetPlaylist();
        delete p;
        delete s1;
        delete s2;
    }

    SECTION("Testing playing at incorrent index") {
        //Arrange
        Playlist* p = new Playlist("Playlist");
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        p->addSong(s1);
        p->addSong(s2);
        PlaylistController::controller().setPlaylist(p);
        auto expected1 = s1;

        //Act
        REQUIRE_THROWS_AS(PlaylistController::controller().playAtIndex(2), std::out_of_range);

        auto result1 = p->currentSong();

        //Assert
        REQUIRE(expected1 == result1);

        AudioPlayer::player().stopPlaying();
        PlaylistController::controller().unsetPlaylist();
        delete p;
        delete s1;
        delete s2;
    }


}
