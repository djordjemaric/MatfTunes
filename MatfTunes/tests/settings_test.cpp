#include <catch.hpp>
#include "../src/settings.h"

TEST_CASE ("Testing Settings Class","[class]"){

    SECTION("Testing setting height"){
        unsigned expected = 600;

        Settings::settings().setHeight(expected);

        unsigned result = Settings::settings().height();

        REQUIRE(expected == result);
    }

    SECTION("Testing setting width"){
        unsigned expected = 800;

        Settings::settings().setWidth(expected);

        unsigned result = Settings::settings().width();

        REQUIRE(expected == result);
    }

    SECTION("Testing setting default dir")
    {
        QString source = "../../data";

        Settings::settings().setDefaultDir(source);

        QString result = Settings::settings().defaultDir();

        REQUIRE(source == result);
    }
}
