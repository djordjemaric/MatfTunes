#include <catch.hpp>
#include "../src/song.h"
#include <taglib/taglib.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>
#include <QDate>

TEST_CASE("Testing Song Class","[class]"){
    SECTION("Testing liking song"){
        Song* song = new Song("1.mp3");
        bool expected = not song->liked();

        song->like();
        bool result = song->liked();

        REQUIRE(result == expected);

        delete song;
    }

    SECTION("Testing incrementing number of time song has been played"){
        Song* song = new Song("1.mp3");
        unsigned expected = song->timesPlayed() + 1;

        song->incrementTimesPlayed();
        unsigned result = song->timesPlayed();

        REQUIRE(result == expected);

        delete song;
    }

    SECTION("Testing seting artist of the song"){
        Song* song = new Song("1.mp3");
        std::string expected = QDate::currentDate().toString().toStdString();

        song->setArtist(QString::fromStdString(expected));
        TagLib::MPEG::File mpegFile(song->path().toUtf8().constData());
        TagLib::ID3v2::Tag *tag = mpegFile.ID3v2Tag();

        std::string result = tag->artist().to8Bit(true);

        REQUIRE(result == expected);

        delete song;
    }

    SECTION("Testing seting name of the song"){
        Song* song = new Song("1.mp3");
        std::string expected = QDate::currentDate().toString().toStdString();

        song->setName(QString::fromStdString(expected));
        TagLib::MPEG::File mpegFile(song->path().toUtf8().constData());
        TagLib::ID3v2::Tag *tag = mpegFile.ID3v2Tag();

        std::string result = tag->title().to8Bit(true);

        REQUIRE(result == expected);

        delete song;
    }

    SECTION("Testing setting path to image of the song"){
        Song* song = new Song("1.mp3");
        QString expected = "random";

        song->setPathToImg(expected);
        QString result = song->pathToImg();

        REQUIRE(result == expected);

        delete song;
    }

    SECTION("Testing getting song path"){
        QString expected = "1.mp3";
        Song* song = new Song(expected);

        QString result = song->path();

        REQUIRE(result == expected);

        delete song;
    }
}
