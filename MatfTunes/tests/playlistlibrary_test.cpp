#include "catch.hpp"
#include "../src/playlistlibrary.h"
#include "../src/playlist.h"
#include "../src/implicitplaylist.h"

TEST_CASE("Testing PlaylistLibrary class", "[class]") {
    SECTION("Testing adding new playlist"){
        Playlist* playlist = new Playlist;
        PlaylistLibrary::playlistLibrary().addPlaylist(playlist);

        QVector<Playlist*> playlists = PlaylistLibrary::playlistLibrary().playlists();
        Playlist* result = playlists.last();

        int num_of_playlists = PlaylistLibrary::playlistLibrary().numberOfPlaylists();
        REQUIRE_FALSE(num_of_playlists == 0);
        REQUIRE(result == playlist);

        PlaylistLibrary::playlistLibrary().removePlaylist(playlist);
    }

    SECTION("Testing adding new playlist via name"){
        QString name;
        Playlist* p = new Playlist(name);
        PlaylistLibrary::playlistLibrary().addPlaylist(p);
        int num_of_playlists = PlaylistLibrary::playlistLibrary().numberOfPlaylists();
        REQUIRE_FALSE(num_of_playlists == 0);

        QVector<Playlist*> playlists = PlaylistLibrary::playlistLibrary().playlists();
        QString result = playlists.last()->name();

        REQUIRE(result == name);

        PlaylistLibrary::playlistLibrary().removePlaylist(p);    }

    SECTION("Testing removing playlist"){
        int num_expected = PlaylistLibrary::playlistLibrary().numberOfPlaylists();

        REQUIRE(num_expected > 0);
        Playlist* p = new Playlist;
        QString name = p->name();

        PlaylistLibrary::playlistLibrary().addPlaylist(p);
        PlaylistLibrary::playlistLibrary().removePlaylist(p);
        Playlist *result_playlist = PlaylistLibrary::playlistLibrary().playlistByName(name);

        int num_result = PlaylistLibrary::playlistLibrary().numberOfPlaylists();

        REQUIRE(result_playlist == nullptr);
        REQUIRE(num_expected == num_result);
    }

    SECTION("Testing getting all playlists"){
        int num = PlaylistLibrary::playlistLibrary().numberOfPlaylists();

        Playlist *p1 = new Playlist("Playlist 1");
        Playlist *p2 = new Playlist("Playlist 2");
        Playlist *p3 = new Playlist("Playlist 3");

        PlaylistLibrary::playlistLibrary().addPlaylist(p1);
        PlaylistLibrary::playlistLibrary().addPlaylist(p2);
        PlaylistLibrary::playlistLibrary().addPlaylist(p3);

        QVector<Playlist*> playlists = PlaylistLibrary::playlistLibrary().playlists();

        REQUIRE(playlists.size() == (num + 3));
        REQUIRE(p1 == playlists[num]);
        REQUIRE(p2 == playlists[num+1]);
        REQUIRE(p3 == playlists[num+2]);

        PlaylistLibrary::playlistLibrary().removePlaylist(p1);
        PlaylistLibrary::playlistLibrary().removePlaylist(p2);
        PlaylistLibrary::playlistLibrary().removePlaylist(p3);
    }

    SECTION("Testing getting number of playlists")
    {
        int num = PlaylistLibrary::playlistLibrary().playlists().size();

        int result = PlaylistLibrary::playlistLibrary().numberOfPlaylists();

        REQUIRE(num == result);
    }

    SECTION("Testing getting playlist by name")
    {
        QString name = "Playlist Test";
        Playlist* expected = new Playlist(name);
        PlaylistLibrary::playlistLibrary().addPlaylist(expected);
        Playlist* result = PlaylistLibrary::playlistLibrary().playlistByName(name);

        REQUIRE(expected == result);

        PlaylistLibrary::playlistLibrary().removePlaylist(expected);
        PlaylistLibrary::playlistLibrary().removePlaylist(result);
    }

    SECTION("Testing getting playlists via token")
    {
        Playlist *p1 = new Playlist("Playlist 1");
        Playlist *p2 = new Playlist("Playlist 2");
        Playlist *p3 = new Playlist("Plist 3");

        PlaylistLibrary::playlistLibrary().addPlaylist(p1);
        PlaylistLibrary::playlistLibrary().addPlaylist(p2);
        PlaylistLibrary::playlistLibrary().addPlaylist(p3);

        QVector<Playlist*> playlists = PlaylistLibrary::playlistLibrary().playlists("plaYLIST");

        REQUIRE(playlists.size() == 2);
        REQUIRE(p1 == playlists[0]);
        REQUIRE(p2 == playlists[1]);

        PlaylistLibrary::playlistLibrary().removePlaylist(p1);
        PlaylistLibrary::playlistLibrary().removePlaylist(p2);
        PlaylistLibrary::playlistLibrary().removePlaylist(p3);
    }
}
