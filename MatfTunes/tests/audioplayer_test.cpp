#include "catch.hpp"
#include "../src/playlist.h"
#include "../src/song.h"
#include "../src/audioplayer.h"


TEST_CASE("Testing AudioPlayer class", "[class]"){

    SECTION("currentSong returns nullptr when no song is set") {
        Song *current_song = AudioPlayer::player().currentSong();

        REQUIRE(current_song == nullptr);
    }

    SECTION("currentSong returns a song that's currently playing") {
        Song *s = new Song("1.mp3");
        AudioPlayer::player().playSong(s);

        Song *current_song = AudioPlayer::player().currentSong();

        REQUIRE(current_song == s);

        delete s;
    }


    SECTION("playSong plays given song"){
        Song *s = new Song("2.mp3");
        AudioPlayer::player().playSong(s);
        
        bool current_is_playing = AudioPlayer::player().playing();
        Song *current_song = AudioPlayer::player().currentSong();

        CHECK(current_is_playing == true);
        REQUIRE(current_song == s);

        delete s;
    }

    SECTION("if current song is set to nullptr, playSong does not play that song") {
        Song *s = nullptr;
        AudioPlayer::player().playSong(s);
        
        bool current_is_playing = AudioPlayer::player().playing();

        REQUIRE(current_is_playing == false);
    }


    SECTION("playing returns true when song is playing"){
        Song *s = new Song("2.mp3");
        AudioPlayer::player().playSong(s);
        
        bool current_is_playing = AudioPlayer::player().playing();

        REQUIRE(current_is_playing == true);

        delete s;
    }

    SECTION("if current song is set to nullptr, playing equals false"){
        Song *s = nullptr;
        AudioPlayer::player().setCurrentSong(s);
        
        bool current_is_playing = AudioPlayer::player().playing();

        REQUIRE(current_is_playing == false);
    }


    SECTION("muted returns AudioPlayer's mute status"){
        bool muted = AudioPlayer::player().muted();
        
        bool current_muted_statud = AudioPlayer::player().muted();

        REQUIRE(current_muted_statud == muted);
    }

    SECTION("setCurrentSong sets given song to AudioPlayer's current song") {
        Song *s = new Song("2.mp3");
        AudioPlayer::player().setCurrentSong(s);

        Song *current_song = AudioPlayer::player().currentSong();

        REQUIRE(current_song == s);

        delete s;
    }

    SECTION("setVolume mutes AudioPlayer when volume <= -100"){
        float volume = -100;
        AudioPlayer::player().setVolume(volume);
        
        bool current_muted_statud = AudioPlayer::player().muted();

        REQUIRE(current_muted_statud == true);
    }

    SECTION("setVolume changes m_audioOutput volumeLevel using logarithimc fomula when volume > -100"){
        float volume = 10;
        double naturalMax = 1000;
        double naturalMin = 0.1;
        double volumeLevel = pow(10, (volume / 1000.0) * log10(naturalMax / naturalMin)) * naturalMin;
        double precision = 10e-4;

        AudioPlayer::player().setVolume(volume);
        bool currentMutedStatus = AudioPlayer::player().muted();
        auto currentVolumeLevel = AudioPlayer::player().volume();

        REQUIRE(currentMutedStatus == false);
        REQUIRE(abs(currentVolumeLevel - volumeLevel) < precision);
    }

    SECTION("invertMuted toggles AudioPlayer's mute status"){
        bool muted_status = AudioPlayer::player().muted();
        AudioPlayer::player().invertMuted();
        
        bool current_muted_statud = AudioPlayer::player().muted();

        REQUIRE(current_muted_statud == not muted_status);
    }

    SECTION("stopPlaying updates AudioPlayer's state to halt audio playback"){
        Song *s = new Song("2.mp3");
        AudioPlayer::player().playSong(s);
        AudioPlayer::player().stopPlaying();
        
        bool current_is_playing = AudioPlayer::player().playing();

        REQUIRE(current_is_playing == false);
    }

    SECTION("stopPlaying updates AudioPlayer's current song to nullptr"){
        Song *s = new Song("2.mp3");
        AudioPlayer::player().playSong(s);
        AudioPlayer::player().stopPlaying();

        Song *current_song = AudioPlayer::player().currentSong();

        REQUIRE(current_song == nullptr);
        \
        delete s;
    }


    SECTION("if current song is set to nullptr, play does not change current song nor playign status") {
        Song *s = nullptr;
        AudioPlayer::player().setCurrentSong(s);
        bool is_playing = AudioPlayer::player().playing();

        AudioPlayer::player().play();
        Song *current_song = AudioPlayer::player().currentSong();
        bool current_is_playing = AudioPlayer::player().playing();

        CHECK(current_song == s);
        REQUIRE(current_is_playing == is_playing);
    }

    SECTION("if m_player is already playing, play does not change current song nor playign status") {
        Song *s = new Song("1.mp3");
        AudioPlayer::player().playSong(s);
        bool is_playing = AudioPlayer::player().playing();

        AudioPlayer::player().play();
        Song *current_song = AudioPlayer::player().currentSong();
        bool current_is_playing = AudioPlayer::player().playing();

        CHECK(current_song == s);
        REQUIRE(current_is_playing == is_playing);

        delete s;
    }

    SECTION("when m_player isn't playing and a song is not equal to nullptr, play initiates playback for the current song") {
        Song *s = new Song("1.mp3");
        AudioPlayer::player().setCurrentSong(s);

        AudioPlayer::player().play();
        Song *current_song = AudioPlayer::player().currentSong();
        bool current_is_playing = AudioPlayer::player().playing();

        CHECK(current_song == s);
        REQUIRE(current_is_playing == true);

        delete s;
    }


    SECTION("if current song is set to nullptr, pause does not change current song nor playing status"){
        Song *s = nullptr;
        AudioPlayer::player().setCurrentSong(s);
        bool is_playing = AudioPlayer::player().playing();
        AudioPlayer::player().pause();

        Song *current_song = AudioPlayer::player().currentSong();
        bool current_is_playing = AudioPlayer::player().playing();


        CHECK(current_song == s);
        REQUIRE(current_is_playing == is_playing);
    }

    SECTION("if m_player is not playing, pause does not change current song nor playign status"){
        AudioPlayer::player().pause();
        
        bool is_playing = AudioPlayer::player().playing();
        AudioPlayer::player().pause();
        
        REQUIRE(AudioPlayer::player().playing() == is_playing);
    }

    SECTION("when m_player is playing and a song is not equal to nullptr, pause stops the song") {
        Song *s = new Song("1.mp3");
        AudioPlayer::player().playSong(s);

        AudioPlayer::player().pause();

        CHECK(AudioPlayer::player().currentSong() == s);
        REQUIRE(AudioPlayer::player().playing() == false);

        delete s;
    }


    SECTION("if current song is set to nullptr, setPosition does not change current m_player's position") {
        Song *s = nullptr;
        AudioPlayer::player().setCurrentSong(s);
        auto previous_position = AudioPlayer::player().position();
        AudioPlayer::player().setPosition(100);
        auto current_position = AudioPlayer::player().position();

        REQUIRE(current_position == previous_position);
    }

    SECTION("if current song is not equal to nullptr, setPosition changes current m_player's position by given formula position=1000*position") {
        Song *s = new Song("2.mp3");
        AudioPlayer::player().setCurrentSong(s);
        auto previous_position = AudioPlayer::player().position();
        AudioPlayer::player().setPosition(100);
        auto current_position = AudioPlayer::player().position();

        REQUIRE(current_position == previous_position*1000);

        delete s;
    }


    SECTION("if loop equals true, setLoop sets m_player to loop infinitly") {
        bool loop = true;
        AudioPlayer::player().setLoop(loop);

        REQUIRE(AudioPlayer::player().loop() == loop);
    }

}
