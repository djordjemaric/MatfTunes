#include <catch.hpp>
#include "../src/song.h"
#include "../src/songlibrary.h"
#include <QDate>

TEST_CASE ("Testing SongLibrary Class","[class]"){
    SECTION ("Testing removing all songs") {
        int expected = 0;

        SongLibrary::songLibrary().removeAllSongs();
        int result = SongLibrary::songLibrary().songs().size();

        REQUIRE(result == expected);
    }

    SECTION ("Testing adding song"){
        Song* song = new Song("1.mp3");
        SongLibrary::songLibrary().addSong(song);

        QVector<Song*> songs = SongLibrary::songLibrary().songs();
        Song* result = songs.last();

        REQUIRE(result == song);

        SongLibrary::songLibrary().removeAllSongs();
    }

    SECTION("Testing adding song via path"){
        QString path = "1.mp3";

        Song* song = SongLibrary::songLibrary().addSongViaPath(path);

        REQUIRE_FALSE(song == nullptr);

        SongLibrary::songLibrary().removeAllSongs();
    }

    SECTION("Testing getting song via path"){
        QString path = "1.mp3";
        Song* song = SongLibrary::songLibrary().addSongViaPath(path);

        Song* result = SongLibrary::songLibrary().song(path);

        REQUIRE(song == result);

        SongLibrary::songLibrary().removeAllSongs();
    }

    SECTION("Testing removing song"){
        QString path = "1.mp3";
        Song* song = SongLibrary::songLibrary().addSongViaPath(path);

        SongLibrary::songLibrary().removeSong(song);
        Song* result = SongLibrary::songLibrary().song(path);

        REQUIRE(result == nullptr);

        SongLibrary::songLibrary().removeAllSongs();
    }

    SECTION("Testing getting songs"){
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        Song* s3 = new Song("1.mp3");

        SongLibrary::songLibrary().addSong(s1);
        SongLibrary::songLibrary().addSong(s2);
        SongLibrary::songLibrary().addSong(s3);

        QVector<Song*> songs = SongLibrary::songLibrary().songs();

        REQUIRE(songs.size() == 3);
        REQUIRE(s1 == songs[0]);
        REQUIRE(s2 == songs[1]);
        REQUIRE(s3 == songs[2]);

        SongLibrary::songLibrary().removeAllSongs();
    }

    SECTION("Testing getting songs with token"){
        Song* s1 = new Song("1.mp3");
        Song* s2 = new Song("1.mp3");
        Song* s3 = new Song("1.mp3");

        SongLibrary::songLibrary().addSong(s1);
        SongLibrary::songLibrary().addSong(s2);
        SongLibrary::songLibrary().addSong(s3);

        s1->setName("name1");
        s2->setName("name2");
        s3->setName("nam1");

        QVector<Song*> songs = SongLibrary::songLibrary().songs("naMe");

        REQUIRE(songs.size() == 2);
        REQUIRE(s1 == songs[0]);
        REQUIRE(s2 == songs[1]);

        SongLibrary::songLibrary().removeAllSongs();
    }

}
