#include "catch.hpp"
#include "math.h"

#include <QDebug>

#include "../src/logslider.h"

TEST_CASE("Testing LogSlider class", "[class]"){


    SECTION("setNaturalMin should change slider minimum value using logarithmic formula") {
        //arange
        int value = 100;
        LogSlider* ls = new LogSlider();
        int new_minimum = static_cast<int>(log10(value)*ls->scale());

        //action
        ls->setNaturalMin(value);

        //assert
        REQUIRE(ls->minimum() == new_minimum);
        delete ls;
    }

    SECTION("setNaturalMax should change slider maximum value using logarithmic formula") {
        //arange
        int value = 100;
        LogSlider* ls = new LogSlider();
        int new_maximum = static_cast<int>(log10(value)*ls->scale());

        //action
        ls->setNaturalMax(value);

        //assert
        REQUIRE(ls->maximum() == new_maximum);
        delete ls;
    }

    SECTION("setNaturalValue should change slider value value using logarithmic formula") {
        //arange
        LogSlider* ls = new LogSlider();
        double value = 10;
        int new_value = static_cast<int>(log10(value)*ls->scale());
        ls->setNaturalMin(0.1);   // Set the minimum value
        ls->setNaturalMax(1000.0); // Set the maximum value

        //action
        ls->setNaturalValue(value);

        //assert
        REQUIRE(ls->value() == new_value);
        delete ls;
    }

}
