#include "logslider.h"
#include "math.h"

#include <QSlider>
#include <QDebug>

LogSlider::LogSlider(QWidget* parent, double scale) : QSlider(parent), m_scale(scale) {
    connect(this, &QSlider::valueChanged, this, &LogSlider::onValueChanged);
    setOrientation(Qt::Horizontal);
}

LogSlider::~LogSlider() {

}

void LogSlider::setNaturalMin(double min)
{
    setMinimum(static_cast<int>(log10(min) * m_scale));
}

void LogSlider::setNaturalMax(double max)
{
    setMaximum(static_cast<int>(log10(max) * m_scale));
}

void LogSlider::setNaturalValue(double val)
{
    setValue(static_cast<int>(log10(val) * m_scale));
}

void LogSlider::onValueChanged(double value)
{
    //qDebug() << value;
    emit naturalValueChanged(value);
}


