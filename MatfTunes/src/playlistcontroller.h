#ifndef PLAYLISTCONTROLLER_H
#define PLAYLISTCONTROLLER_H

#include <QObject>
#include "playlist.h"


class PlaylistController : public QObject
{
    Q_OBJECT
public:
    explicit PlaylistController(QObject *parent = nullptr);
    static PlaylistController& controller();
    void setPlaylist(Playlist* p);
    inline Playlist* playlist() const { return m_currentPlaylist; };
    void playPlaylist();
    inline bool shuffle() const { return m_shuffle; }
    void playAtIndex(int index);
public slots:
    void nextSong();
    void previousSong();
    void toggleShuffle();
    void songFinished();
    void unsetPlaylist();
    void playlistDeleted(Playlist*);
signals:

private:
    Playlist* m_currentPlaylist = nullptr;
    bool m_shuffle = false;

};

#endif // PLAYLISTCONTROLLER_H
