#include "playlistcontroller.h"
#include "audioplayer.h"

PlaylistController::PlaylistController(QObject *parent)
    : QObject{parent}
{
    connect(&AudioPlayer::player(), &AudioPlayer::songFinished, this, &PlaylistController::songFinished);
}

PlaylistController &PlaylistController::controller()
{
    static PlaylistController controller{};
    return controller;
}

void PlaylistController::setPlaylist(Playlist* p) {
    unsetPlaylist();
    m_currentPlaylist = p;
    m_shuffle = false;
}

void PlaylistController::unsetPlaylist() {
    if (m_currentPlaylist != nullptr) {
        m_currentPlaylist->setCurrentIndex(0);
        m_currentPlaylist = nullptr;
    }
}

void PlaylistController::playPlaylist() {
    try {
        auto currentSong = m_currentPlaylist->currentSong();
        AudioPlayer::player().playSong(currentSong);
    } catch (const std::exception& e) {
        qDebug() << "Empty playlist";
    }
}

void PlaylistController::nextSong() {
    if (m_currentPlaylist == nullptr)
        return;
    Song* nextSong;
    if (m_shuffle) {
        nextSong = m_currentPlaylist->shuffle();
    } else {
        nextSong = m_currentPlaylist->next();
    }
    AudioPlayer::player().playSong(nextSong);
}

void PlaylistController::previousSong() {
    if (m_currentPlaylist == nullptr)
        return;
    Song* nextSong;
    if (m_shuffle) {
        nextSong = m_currentPlaylist->shuffle();
    } else {
        nextSong = m_currentPlaylist->previous();
    }
    AudioPlayer::player().playSong(nextSong);
}

void PlaylistController::toggleShuffle() {
    m_shuffle = !m_shuffle;
}

void PlaylistController::songFinished() {
    if (m_currentPlaylist == nullptr)
        return;
    AudioPlayer::player().stopPlaying();
    Song* nextSong;
    if (m_shuffle) {
        nextSong = m_currentPlaylist->shuffle();
    } else {
        nextSong = m_currentPlaylist->next();
    }
    AudioPlayer::player().playSong(nextSong);
}

void PlaylistController::playAtIndex(int index) {
    if (index < 0 || index >= m_currentPlaylist->numOfSongs()) {
        throw std::out_of_range("Song index is out of range!");
    }
    m_currentPlaylist->setCurrentIndex(index);
    auto currentSong = m_currentPlaylist->currentSong();
    AudioPlayer::player().playSong(currentSong);
}

void PlaylistController::playlistDeleted(Playlist* p) {
    if (p == m_currentPlaylist) {
        AudioPlayer::player().stopPlaying();
        m_currentPlaylist = nullptr;
    }
}
