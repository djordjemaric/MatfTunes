#include "editsongui.h"
#include "ui_editsongui.h"
#include <QFileDialog>

EditSongUI::EditSongUI(Song* song, SongItemUI* songItem, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditSongUI),
    m_song(song),
    m_songItem(songItem)
{
    ui->setupUi(this);
    setupUI();

    ui->artistEdit->setText(m_song->artist());
    ui->nameEdit->setText(m_song->name());
    ui->leLyrics->setText(m_song->lyrics());

    connect(ui->coverEdit, &QAbstractButton::clicked, this, &EditSongUI::change_icon);
    connect(ui->saveChanges, &QAbstractButton::clicked, this, &EditSongUI::save_changes);
}

EditSongUI::~EditSongUI()
{
    delete ui;
}

void EditSongUI::setupUI(){
    this->setWindowTitle("Edit Song");
    this->setStyleSheet("background:#000014;border:none;");
    ui->coverEdit->setIcon(QIcon(":resources/add_img.png"));

    ui->horizontalLayout->setStretchFactor(ui->nameLabel, 1);
    ui->horizontalLayout->setStretchFactor(ui->nameEdit, 1);
    ui->horizontalLayout_2->setStretchFactor(ui->artistLabel, 1);       // 1 unit of stretch
    ui->horizontalLayout_2->setStretchFactor(ui->artistEdit, 1);
}

void EditSongUI::change_icon()
{
    newPathToImg = QFileDialog::getOpenFileName(this, "Choose Cover", "../..", tr("Images (*.png *.xpm *.jpg *.jpeg)"));
}

void EditSongUI::save_changes(){

    QString name = ui->nameEdit->text();
    QString artist = ui->artistEdit->text();
    QString lyrics = ui->leLyrics->text();

    m_song->setArtist(artist);
    m_song->setName(name);
    m_song->setLyrics(lyrics);

    if (newPathToImg != "") {
        m_song->setPathToImg(newPathToImg);
    }

    m_songItem->updateSongDetails();
    emit savedSongChanges(m_song);

    this->close();
    delete this;
}
