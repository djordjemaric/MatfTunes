#include "lyricssearchthread.h"
#include <QMutexLocker>
LyricsSearchThread::LyricsSearchThread(Song* song, QString lyrics, QObject *parent)
    : QThread{parent},
    m_song(song),
    m_lyrics(lyrics)
{}

void LyricsSearchThread::run() {
    QString song_lyrics = m_song->lyrics();

    QString z = m_lyrics + "#" + song_lyrics;
    z = z.toLower();
    QVector<int> z_array = zet(z);

    for (int i = m_lyrics.size()+1; i<z.size(); i++) {
        if (z_array[i] == m_lyrics.size()) {
            emit finishedSearch(m_song);
            return;
        }
    }
    emit finishedSearch(nullptr);
}

QVector<int> LyricsSearchThread::zet(QString& s) {
    int n = s.size();
    QVector<int> z(n);

    int l = 0, r = 0;

    for (int i = 1; i<n; i++) {
        if (i <= r) {
            z[i] = std::min(z[i-l], r-i+1);
        }
        while (i + z[i] < n && s[i + z[i]] == s[z[i]]) {
            z[i]++;
        }

        if (i + z[i] - 1 > r) {
            l = i;
            r = i + z[i] - 1;
        }
    }

    return z;
}
