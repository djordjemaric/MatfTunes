#ifndef SONGLIBRARYWINDOW_H
#define SONGLIBRARYWINDOW_H

#include <QMainWindow>
#include "songitemui.h"
#include "playlist.h"

namespace Ui {
class SongLibraryWindow;
}

class SongLibraryWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SongLibraryWindow(Playlist* p, QWidget *parent = nullptr);
    ~SongLibraryWindow();

private slots:
    void display_song(Song*);
    void update_song_list();
    void choose_song();
    void select_songs();
    void add_multiple_songs();
signals:
    void songChosen(Song*, Playlist*);
    void songs_chosen(QVector<Song*>, Playlist*);

private:
    Ui::SongLibraryWindow *ui;
    Playlist* m_playlist;
};

#endif // SONGLIBRARYWINDOW_H
