#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <QMediaPlayer>
#include <QAudioOutput>
#include "song.h"


class AudioPlayer : public QObject
{
    Q_OBJECT
public:
    static AudioPlayer& player();

    void playSong(Song* s);
    Song* currentSong() const;
    inline bool muted() const { return m_muted; }
    inline bool playing() const { return m_player->isPlaying(); }
    inline int position() const { return m_player->position(); }
    inline bool loop() const { return m_player->loops() == QMediaPlayer::Infinite; }
    inline float volume() const { return m_audioOutput->volume(); }
    void setCurrentSong(Song *newCurrentSong);
private:
    AudioPlayer();
    ~AudioPlayer();
    AudioPlayer(const AudioPlayer&) = delete;
    AudioPlayer& operator=(const AudioPlayer&) = delete;

    QAudioOutput* m_audioOutput;
    Song* m_currentSong;
    QMediaPlayer* m_player;
    bool m_muted = false;
public slots:
    void setVolume(float);
    void invertMuted();
    void stopPlaying();
    void play();
    void pause();
    void setPosition(int64_t);
    void setLoop(bool);
private slots:
    void positionChanged(int64_t);
    void statusChanged(QMediaPlayer::MediaStatus);
signals:
    void songChanged(Song*);
    void positionUpdated(int64_t);
    void mutedChanged(bool);
    void songFinished();
};

#endif // AUDIOPLAYER_H
