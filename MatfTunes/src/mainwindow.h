#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QMutex>
#include "editsongui.h"
#include "songlibrary.h"
#include "playlistitemui.h"
#include "songitemui.h"
#include "playlist.h"
#include "playeritemui.h"
#include "playlistcontroller.h"
#include "threadhandler.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum class Pages
{
    MainPage,
    AudioPlayerPage,
    SongLibPage,
    PlaylistsPage
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

signals:
    void deletedSong(Song*);
    void refreshLiked();
    void playlistDeleted(Playlist*);
private slots:
    // General
    void backButton();
    void settingsButton();

    // Main Page
    void songsMainPageButton();
    void playlistsMainPageButton();
    void addSongButton();
    void addPlaylistButton();

    // Playlists page
    void showCreateNewPlaylist();
    void hideCreateNewPlaylist();
    void saveNewPlaylist();
    void removeSongFromPlaylist(Song*);
    void refreshLikedShow();
    void removePlaylist(PlaylistItemUI*);
    void showPlaylistSongs(PlaylistItemUI*);
    void openLibrary();
    void playPlaylist();
    void editPlaylist(PlaylistItemUI*);
    void updatePlaylistDetails(PlaylistItemUI*);
    void receiveLike(Song*);
    void receiveSong(Song*, Playlist*);
    void receiveSongs(QVector<Song *>, Playlist*);

    // SongItem
    void removeSong(SongItemUI*, QListWidget*);
    void editSong(SongItemUI*);

    // Display
    void displaySong(Song*, QListWidget*, Playlist* p = nullptr);
    void displayPlaylist(Playlist*);

    // Filter
    void filterSongItemUI(QString);
    void filterPlaylistItemUI(QString);

    // Search
    void searchSongsChangeUI();
    void searchPlaylistsChangeUI();
    void onThreadFinish(Song *);
    void searchLyrics();

    // Player
    void closePlayer();
    void fullScreenPlayer(bool);
    void changePlayerVisible(bool);
    void updatePlayerSongInfo(Song *);
    void showSongLyrics();

private:
    Ui::MainWindow *ui;
    PlayerItemUI* m_playeritem;
    ThreadHandler* m_thread_handler;
    int m_lastVisitedPage=0;

    void setupIcons();
    void setupPlayer();
    void setupMainPage();
    void setupSongLibPage();
    void setupPlaylistPage();
    void clearListWidget(QListWidget *listWidget);
    void setupTopTracks();
    void reloadSongLibrary();
    void reloadPlaylists();

    void saveCurrPageAndSetNext(Pages page);
    void showPlaylistCover(const QString path);
    void addRemoveLiked(Song*);
    Playlist* getCurrentPlaylist() const;
    void reloadCurrentPlaylist();
    void updateAllSongsPlaylist(Song*, bool);

    void setCurrentPlaylist(QString name);
};
#endif // MAINWINDOW_H
