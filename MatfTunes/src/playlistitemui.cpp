#include "playlistitemui.h"
#include "ui_playlistitemui.h"
#include "implicitplaylist.h"
#include <QAbstractButton>
#include <QListWidgetItem>
#include <QPixmap>

PlaylistItemUI::PlaylistItemUI(Playlist* p, QListWidgetItem* item, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlaylistItemUI)
{
    m_playlist = p;
    m_item = item;
    m_icon_path = p->coverPath();
    ui->setupUi(this);
    this->setStyleSheet("border:none");

    ui->name->setText(p->name());
    connect(ui->rmPlBtn, &QPushButton::clicked, this, &PlaylistItemUI::removePlaylist);
    connect(ui->editPlBtn, &QAbstractButton::clicked, this, &PlaylistItemUI::edit_playlist);

    //    TODO: ui->rmPlBtn->setVisible(true); for liked songs and all songs
    ui->rmPlBtn->setStyleSheet("color: #ba718a;");
    ui->rmPlBtn->setIcon(QIcon(":/resources/trash.png"));
    ui->rmPlBtn->setStyleSheet("color: #ba718a;");
    ui->editPlBtn->setIcon(QIcon(":/resources/settings.png"));


    if (dynamic_cast<ImplicitPlaylist*>(m_playlist)) {
        hideForImplicit();
    }

    QPixmap pix(p->coverPath());
    ui->imgLabel->setAlignment(Qt::AlignCenter);
    ui->imgLabel->setFixedSize(40, 40);
    ui->imgLabel->setPixmap(pix.scaled(ui->imgLabel->width(),ui->imgLabel->width(),Qt::KeepAspectRatio));
}

PlaylistItemUI::~PlaylistItemUI()
{
    delete ui;
}

void PlaylistItemUI::removePlaylist()
{
    emit deletePlaylistItem(this);
}

void PlaylistItemUI::edit_playlist()
{
    emit edit_playlist_item(this);
}

void PlaylistItemUI::edit_playlist_name()
{
    ui->name->setText(m_playlist->name());
}

void PlaylistItemUI::set_playlist_icon(QString &new_path)
{
    QPixmap pix(new_path);
    ui->imgLabel->setPixmap(pix.scaled(ui->imgLabel->width(),ui->imgLabel->width(),Qt::KeepAspectRatio));
}

void PlaylistItemUI::hideForImplicit() {
    ui->rmPlBtn->hide();
    ui->editPlBtn->hide();
}
