#include "songitemui.h"
#include "ui_songitemui.h"
#include "audioplayer.h"
#include "playlistcontroller.h"
#include <QIcon>
#include <QTime>

SongItemUI::SongItemUI(Song* s, QListWidgetItem* list_item, QListWidget* list, Playlist* p, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::SongItemUI)
    , m_song(s)
    , m_list(list)
    , m_list_item(list_item)
    , m_playlist(p)
{
    m_list_item->setBackground(QBrush(QColor(63,69,100)));

    ui->setupUi(this);
    this->setStyleSheet("background-color: #3f4564; border: none;");

    ui->lArtist->setStyleSheet("background:none");
    ui->lDuration->setStyleSheet("background:none");
    ui->lTitle->setStyleSheet("background:none");
    ui->playButton->setStyleSheet("background:none");
    ui->removeButton->setStyleSheet("background:none");
    ui->editButton->setStyleSheet("background:none");

    ui->lTitle->setText(m_song->name());
    ui->lArtist->setText(m_song->artist());
    int durationSeconds = m_song->duration();
    ui->lDuration->setText(QTime::fromMSecsSinceStartOfDay(durationSeconds*1000).toString("mm:ss"));
    ui->playButton->setIcon(QIcon(":/resources/play.png"));
    if (AudioPlayer::player().currentSong() == m_song) {
        ui->playButton->setIcon(QIcon(":/resources/wave.png"));
    }
    ui->removeButton->setIcon(QIcon(":/resources/trash.png"));
    ui->editButton->setIcon(QIcon(":/resources/settings.png"));
    connect(ui->playButton, &QAbstractButton::clicked, this, &SongItemUI::play_song);
    connect(ui->removeButton, &QAbstractButton::clicked, this, &SongItemUI::remove_song);
    connect(ui->editButton, &QAbstractButton::clicked, this, &SongItemUI::edit_song);
    connect(&AudioPlayer::player(), &AudioPlayer::songChanged, this, &SongItemUI::changeIcon);

}

SongItemUI::~SongItemUI()
{
    delete ui;
}

void SongItemUI::updateSongDetails()
{
    ui->lTitle->setText(m_song->name());
    ui->lArtist->setText(m_song->artist());
}

void SongItemUI::hideForImplicit()
{
    ui->removeButton->hide();
    ui->editButton->hide();
}

void SongItemUI::changeIcon(Song* s) {
    if (s == m_song) {
        ui->playButton->setIcon(QIcon(":/resources/wave.png"));
        return;
    }
    ui->playButton->setIcon(QIcon(":/resources/play.png"));
}

void SongItemUI::play_song() {
    if (m_list->objectName() == "playlistSongs") {
        int songIndex = m_list->indexFromItem(m_list_item).row();
        try {
            if (PlaylistController::controller().playlist() == m_playlist) {
                PlaylistController::controller().playAtIndex(songIndex);
            } else {
                PlaylistController::controller().unsetPlaylist();
                PlaylistController::controller().setPlaylist(m_playlist);
                PlaylistController::controller().playAtIndex(songIndex);
            }
        } catch (const std::out_of_range& ex) {
            qDebug() << ex.what();
        }
    } else {
        PlaylistController::controller().unsetPlaylist();
        AudioPlayer::player().playSong(m_song);
    }
}

void SongItemUI::remove_song()
{
    emit deleteSongItem(this,m_list);
}

void SongItemUI::edit_song()
{
    emit edit_song_clicked(this);
}

