#ifndef SONG_H
#define SONG_H

#include <QMediaPlayer>
#include "../serialization/serializable.h"

class Song : public Serializable{
public:
    Song();
    Song(QString path);
    Song(QString path,QString name,QString artist,int duration);
    ~Song();
    inline QString path() const { return m_path; }
    inline QString name() const { return m_name; }
    inline QString artist() const {return m_artist; }
    inline QString lyrics() const { return m_lyrics; }
    inline unsigned duration() const { return m_duration; }
    inline unsigned timesPlayed() const { return m_timesPlayed; }
    inline QString pathToImg() const { return m_path_to_img; }
    void incrementTimesPlayed();
    inline bool liked() const { return m_liked; }
    void like();

    void setName(QString name);
    void setArtist(QString artist);
    void setPathToImg(QString pathToImg);
    void setLyrics(QString lyrics);


    //Serializable
    QVariant toVariant() final;
    void fromVariant(const QVariant &variant) final;

private:
    QString m_path;
    QString m_name;
    QString m_artist;
    QString m_lyrics;
    unsigned m_timesPlayed;
    unsigned m_duration;
    bool m_liked;
    QString m_path_to_img;
};

#endif // SONG_H
