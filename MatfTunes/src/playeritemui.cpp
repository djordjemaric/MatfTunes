#include "playeritemui.h"
#include "ui_playeritemui.h"
#include "audioplayer.h"
#include "playlistcontroller.h"
#include "logslider.h"
#include "songitemui.h"

#include <QIcon>
#include <QTime>

PlayerItemUI::PlayerItemUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlayerItemUI)
{
    ui->setupUi(this);
    ui->horizontalLayout_5->setStretchFactor(ui->verticalLayout_2,1);
    ui->horizontalLayout_5->setStretchFactor(ui->horizontalLayout_2,1);
    ui->horizontalLayout_5->setStretchFactor(ui->horizontalLayout_6,1);
    this->setAutoFillBackground(true);
    this->setStyleSheet("background-color:#000014;");

    setupIcons();
    connectButtons();
    connectSlots();
}


PlayerItemUI::~PlayerItemUI()
{
    delete ui;
}

void PlayerItemUI::setupIcons() {
    ui->play_stop_button->setStyleSheet("border:none");
    ui->next_button->setStyleSheet("border:none");
    ui->loop_button->setStyleSheet("border:none");
    ui->previous_button->setStyleSheet("border:none");
    ui->shuffle_button->setStyleSheet("border:none");
    ui->full_screen_button->setStyleSheet("border:none");
    ui->volume_button->setStyleSheet("border:none");
    ui->x_button->setStyleSheet("border:none");

    ui->play_stop_button->setIcon(QIcon(":/resources/pause.png"));
    ui->next_button->setIcon(QIcon(":/resources/next.png"));
    ui->previous_button->setIcon(QIcon(":/resources/previous.png"));
    ui->loop_button->setIcon(QIcon(":/resources/loop.png"));
    ui->shuffle_button->setIcon(QIcon(":/resources/shuffle.png"));
    ui->volume_button->setIcon(QIcon(":/resources/full_volume.png"));
    ui->full_screen_button->setIcon(QIcon(":/resources/enlarge.png"));
    ui->x_button->setIcon(QIcon(":/resources/x.png"));
    ui->heart_button->setIcon(QIcon(":/resources/heart_empty.png"));
}

void PlayerItemUI::connectButtons() {
    connect(ui->play_stop_button, &QAbstractButton::clicked, this, &PlayerItemUI::play_pause);
    connect(ui->x_button, &QAbstractButton::clicked, this, &PlayerItemUI::closePlayer);
    connect(&AudioPlayer::player(), &AudioPlayer::positionUpdated, this, &PlayerItemUI::updatePosition);
    connect(ui->volume_button, &QAbstractButton::clicked, this, &PlayerItemUI::muteClicked);
    connect(ui->next_button, &QAbstractButton::clicked, this, &PlayerItemUI::next);
    connect(ui->previous_button, &QAbstractButton::clicked, this, &PlayerItemUI::previous);
    connect(ui->shuffle_button, &QAbstractButton::clicked, this, &PlayerItemUI::shuffle);
    connect(ui->loop_button, &QAbstractButton::clicked, this, &PlayerItemUI::loops);
    connect(ui->full_screen_button, &QAbstractButton::clicked, this, &PlayerItemUI::full_screen_player);
    connect(ui->heart_button, &QAbstractButton::clicked, this, &PlayerItemUI::like_song);
    connect(ui->song_slider, &QSlider::sliderMoved, this, &PlayerItemUI::updateMovedPosition);
}

void PlayerItemUI::connectSlots() {
    connect(this, &PlayerItemUI::positionChanged, &AudioPlayer::player(), &AudioPlayer::setPosition);
    LogSlider *volume_slider = new LogSlider(ui->slider_widget, 100);
    volume_slider->setStyleSheet("QSlider::groove:horizontal {height: 13px; background: #00514f;margin: 2px 0;} QSlider::handle:horizontal { background: #00514f; border: 2px solid #6ed4a9; border-radius: 3px;width: 20px;margin: -2px 0;border-radius: 3px;}QSlider::add-page:horizontal {margin: 2px 0;background: #4fb09d;}");

    volume_slider->setNaturalMin(0.1);   // Set the minimum value
    volume_slider->setNaturalMax(1000.0); // Set the maximum value
    volume_slider->setNaturalValue(10.0);

    connect(volume_slider, &LogSlider::naturalValueChanged, &AudioPlayer::player(), &AudioPlayer::setVolume);
    connect(&AudioPlayer::player(), &AudioPlayer::mutedChanged, this, &PlayerItemUI::changeAudioIcon);
    connect(&AudioPlayer::player(), &AudioPlayer::songChanged, this, &PlayerItemUI::updateData);
    connect(this, &PlayerItemUI::loopChanged, &AudioPlayer::player(), &AudioPlayer::setLoop);
    connect(this, &PlayerItemUI::nextSong, &PlaylistController::controller(), &PlaylistController::nextSong);
    connect(this, &PlayerItemUI::previousSong, &PlaylistController::controller(), &PlaylistController::previousSong);
    connect(this, &PlayerItemUI::toggleShuffle, &PlaylistController::controller(), &PlaylistController::toggleShuffle);
    connect(this, &PlayerItemUI::close_player_signal, &PlaylistController::controller(), &PlaylistController::unsetPlaylist);
}

void PlayerItemUI::updateData(Song *s) {
    if (s == nullptr) {
        closePlayer();
        return;
    }
    m_current_song = s;
    m_playing = 1;
    ui->play_stop_button->setIcon(QIcon(":/resources/pause.png"));
    if(s->liked())
        ui->heart_button->setIcon(QIcon(":/resources/heart_full.png"));
    else
        ui->heart_button->setIcon(QIcon(":/resources/heart_empty.png"));

    setSongDetails(s);
    ui->lDuration->setText(QTime::fromMSecsSinceStartOfDay(s->duration()*1000).toString("mm:ss"));
    ui->song_slider->setMaximum(s->duration());
    emit songChanged(s);
}

void PlayerItemUI::setSongDetails(Song* s) {
    ui->lSongName->setText(m_current_song->name());
    ui->lArtistName->setText(m_current_song->artist());
    QPixmap pix(s->pathToImg());
    ui->song_img_label->setAlignment(Qt::AlignCenter);
    ui->song_img_label->setFixedSize(50, 50);
    ui->song_img_label->setPixmap(pix.scaled(ui->song_img_label->width(),ui->song_img_label->width(),Qt::KeepAspectRatio));
}

void PlayerItemUI::play_pause()
{
    if(this->m_playing)
    {
        ui->play_stop_button->setIcon(QIcon(":/resources/play.png"));
        AudioPlayer::player().pause();
    } else
    {
        ui->play_stop_button->setIcon(QIcon(":/resources/pause.png"));
        AudioPlayer::player().play();
    }
    this->m_playing = not this->m_playing;
}

void PlayerItemUI::closePlayer()
{
    AudioPlayer::player().stopPlaying();
    m_current_song = nullptr;
    emit close_player_signal();
}

void PlayerItemUI::updatePosition(int position) {
    // TODO:  UPDATE SONG SLIDER

    ui->song_slider->setValue(position);
    ui->lTime->setText(QTime::fromMSecsSinceStartOfDay(position*1000).toString("mm:ss"));
}

void PlayerItemUI::updateMovedPosition(int position){
    //qDebug() << position;
    ui->song_slider->setValue(position);
    ui->lTime->setText(QTime::fromMSecsSinceStartOfDay(position*1000).toString("mm:ss"));
    emit positionChanged(position);
}


void PlayerItemUI::muteClicked() {
    AudioPlayer::player().invertMuted();
    bool muted = AudioPlayer::player().muted();
    if (muted) {
        ui->volume_button->setIcon(QIcon(":/resources/no_volume.png"));
    } else {
        ui->volume_button->setIcon(QIcon(":/resources/full_volume.png"));
    }
}

void PlayerItemUI::next() {
    emit nextSong();
}

void PlayerItemUI::previous() {
    emit previousSong();
}

void PlayerItemUI::shuffle() {
    m_shuffle = !m_shuffle;
    if(m_shuffle){
        ui->shuffle_button->setIcon(QIcon(":/resources/shuffle_active.png"));
    }
    else {
        ui->shuffle_button->setIcon(QIcon(":/resources/shuffle.png"));
    }
    emit toggleShuffle();
}

void PlayerItemUI::loops() {
    m_loop = !m_loop;
    if(m_loop){
        ui->loop_button->setIcon(QIcon(":/resources/loop_active.png"));
    }
    else {
        ui->loop_button->setIcon(QIcon(":/resources/loop.png"));
    }
    emit loopChanged(m_loop);
}

void PlayerItemUI::full_screen_player(){
    emit full_screen_signal(this->m_full_screen);

    if(this->m_full_screen)
    {
        ui->x_button->setVisible(true);
        ui->full_screen_button->setIcon(QIcon(":/resources/enlarge.png"));
    } else
    {
        ui->x_button->setVisible(false);
        ui->full_screen_button->setIcon(QIcon(":/resources/compress.png"));
    }

    this->m_full_screen = not this->m_full_screen;
}


void PlayerItemUI::like_song(){

    emit liked_song_signal(m_current_song);

    if(!m_current_song->liked()){
        ui->heart_button->setIcon(QIcon(":/resources/heart_empty.png"));
    }
    else {
        ui->heart_button->setIcon(QIcon(":/resources/heart_full.png"));
    }
}

void PlayerItemUI::changeAudioIcon(bool m_muted){
    // TODO: add low volume icon
    if(m_muted)
        ui->volume_button->setIcon(QIcon(":/resources/no_volume.png"));
    else
        ui->volume_button->setIcon(QIcon(":/resources/full_volume.png"));

}

void PlayerItemUI::songDeleted(Song* s) {
    if (s == m_current_song) {
        closePlayer();
    }
}

void PlayerItemUI::updateSongDetails(Song* s) {
    if (s != m_current_song)
        return;
    setSongDetails(s);
    emit songChanged(s);
}
