#include "implicitplaylist.h"

ImplicitPlaylist::ImplicitPlaylist() {}

ImplicitPlaylist::ImplicitPlaylist(QString name) : Playlist(name) {
    if (name == "Liked Songs")
        m_cover_path = ":resources/heart_full.png";
    else if (name == "All songs")
        m_cover_path = ":resources/matfTunes_LOGO_inv.png";
}

void ImplicitPlaylist::changeName(QString) {
    throw std::logic_error("can't change implicit playlist name");
}

void ImplicitPlaylist::changeCover(QString) {
    throw std::logic_error("can't change implicit playlist cover");
}
