#include "settings.h"

Settings::Settings() {
    JsonSerializer::m_dir = "";
    m_serializer = new JsonSerializer();
    m_serializer->load(*this, "settings.json");
    JsonSerializer::m_dir = m_defaultDir;
}

Settings::~Settings() {
    JsonSerializer::m_dir = "";
    m_serializer->save(*this,"settings.json");
    JsonSerializer::m_dir = m_defaultDir;
    delete m_serializer;
}

Settings& Settings::settings() {
    static Settings settings {};
    return settings;
}

void Settings::setDefaultDir(QString& dir) {
    qDebug() << "New default dir is " + dir;
    m_defaultDir = dir;
    JsonSerializer::m_dir = m_defaultDir;
}

void Settings::setHeight(unsigned h) {
    m_height = h;
}

void Settings::setWidth(unsigned w) {
    m_width = w;
}

QVariant Settings::toVariant() {
    QVariantMap map;

    map.insert("defaultDir", m_defaultDir);
    map.insert("width", m_width);
    map.insert("height", m_height);

    return map;
}

void Settings::fromVariant(const QVariant &variant) {
    QVariantMap map = variant.toMap();

    m_defaultDir = map.value("defaultDir").toString();
    m_width = map.value("width").toUInt();
    m_height = map.value("height").toUInt();
}


