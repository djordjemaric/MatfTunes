#include "songlibrary.h"
#include "song.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "../serialization/jsonserializer.h"
#include <fstream>

QVector<Song*> SongLibrary::songs(QString token)
{
    QVector<Song*> songsToShow;
    for (Song* song : m_songs){
        QString name = song->name().toLower();
        QString artist = song->artist().toLower();
        if(name.contains(token.toLower()) or artist.contains(token.toLower())){
            songsToShow.append(song);
        }
    }
    return songsToShow;
}

QVector<Song *> SongLibrary::topNTracks(int n)
{
    QVector<Song*> topNtracks;

    std::sort(m_songs.begin(), m_songs.end(), [](const Song* a, const Song* b) {
        return a->timesPlayed() > b->timesPlayed();
    });

    int numOfTracks = std::min(n, static_cast<int>(m_songs.size()));
    for(int i = 0; i < numOfTracks; i++){
        topNtracks.push_back(m_songs[i]);
    }

    return topNtracks;
}

SongLibrary::SongLibrary()
{
    loadSongs();
}

SongLibrary::~SongLibrary()
{
    saveSongs();
    qDeleteAll(m_songs);
}


SongLibrary& SongLibrary::songLibrary()
{
    static SongLibrary songLibrary {};
    return songLibrary;
}

Song* SongLibrary::addSongViaPath(QString &path)
{
    Song *newSong = new Song(path);

    if (song(path) != nullptr){
        return nullptr;
    }

    m_songs.push_back(newSong);
    return newSong;
}

void SongLibrary::addSong(Song *song){
    m_songs.append(song);
}

Song* SongLibrary::song(const QString& path){
    int numberOfSongs = this->m_songs.length();
    for(int i = 0;i < numberOfSongs; i++){
        if(this->m_songs[i]->path() == path){
            return this->m_songs[i];
        }
    }

    return nullptr;
}

void SongLibrary::removeSong(const Song *song)
{
    for (int i = 0; i < m_songs.size(); i++)
    {
        if (m_songs[i] == song)
        {
            m_songs.remove(i);
            delete song;
            song = nullptr;
            return;
        }
    }
}

void SongLibrary::removeAllSongs(){
    for (Song* song : m_songs) {
        delete song;
    }
    m_songs.clear();
}

QVariant SongLibrary::toVariant()
{
    QVariantList list;

    for(Song* song : this->m_songs){
        QVariant map = song->toVariant();
        list.append(map);
    }
    return list;
}

void SongLibrary::fromVariant(const QVariant &variant)
{
    auto list = variant.toList();
    this->m_songs.clear();
    QList<Song *> songs;

    for(const QVariant &map : list){
        Song *song = new Song();
        song->fromVariant(map);
        std::ifstream f(song->path().toStdString());
        if (f.is_open()) {
            songs.append(song);
        }
    }
    m_songs = songs;
}

void SongLibrary::saveSongs()
{
    JsonSerializer *serializer = new JsonSerializer();
    serializer->save(*this , "saved_songs.json");
    delete serializer;
}

void SongLibrary::loadSongs()
{
    JsonSerializer *serializer = new JsonSerializer();
    serializer->load(*this , "saved_songs.json");
    delete serializer;
}
