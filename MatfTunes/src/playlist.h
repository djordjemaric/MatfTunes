#ifndef PLAYLIST_H
#define PLAYLIST_H

#include "song.h"
#include <QPixmap>

class Playlist : public Serializable
{

public:
    Playlist();
    Playlist(QString name);
    virtual ~Playlist();

    bool addSong(Song* s);

    inline QString name() const { return m_name; }
    Song* currentSong() const;
    inline int currentIndex() const { return m_currentIndex; }
    void setCurrentIndex(int);
    inline int numOfSongs() const { return m_songs.length(); }

    virtual void changeName(QString name);
    virtual void changeCover(QString path);

    inline QVector<Song*> songs() { return m_songs; }

    //Serializable
    QVariant toVariant() final;
    void fromVariant(const QVariant &variant) final;

    inline QString coverPath() const { return m_cover_path; }

    Song* next();
    Song* previous();
    Song* shuffle();

signals:
    void current_index_changed(int index);
    void current_song_changed(const Song &s);
    void playback_state_changed(QMediaPlayer::PlaybackState new_state);
public slots:
    void removeSong(Song *s);

private:
    QString m_name;
    int m_currentIndex;
    QVector<Song*> m_songs;
protected:
    QString m_cover_path = ":resources/default_artist_img.jpg";
};

#endif // PLAYLIST_H
