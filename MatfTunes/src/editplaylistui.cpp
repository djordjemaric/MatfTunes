#include "editplaylistui.h"
#include "ui_editplaylistui.h"

EditPlaylistUI::EditPlaylistUI(Playlist* playlist, PlaylistItemUI* item, QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::EditPlaylistUI),
    m_playlist(playlist),
    m_playlist_item(item)
{
    ui->setupUi(this);

    ui->line_edit_playlist_name->setText(m_playlist->name());

    setupUI();

    connect(ui->btn_save_playlist, &QAbstractButton::clicked, this, &EditPlaylistUI::save_edited_playlist);
    connect(ui->btn_new_playlist_cover, &QAbstractButton::clicked, this, &EditPlaylistUI::change_icon);
}

EditPlaylistUI::~EditPlaylistUI()
{
    delete ui;
}

void EditPlaylistUI::setupUI(){
    m_playlist_item->setStyleSheet("border: none;");
    this->setWindowTitle("Edit Playlist");
    ui->btn_new_playlist_cover->setIcon(QIcon(":resources/add_img.png"));
}

void EditPlaylistUI::change_icon()
{
    newPathToImg = QFileDialog::getOpenFileName(this, "Choose Cover", "../..", tr("Images (*.png *.xpm *.jpg *.jpeg)"));
}

void EditPlaylistUI::save_edited_playlist()
{
    try {
        QString new_name = ui->line_edit_playlist_name->text();
        m_playlist->changeName(new_name);
        m_playlist_item->edit_playlist_name();

        if(newPathToImg != "")
        {
            m_playlist_item->set_playlist_icon(newPathToImg);
            m_playlist->changeCover(newPathToImg);
            emit saved_changes(m_playlist_item);
        }
    } catch (const std::logic_error& l) {
        qDebug() << l.what();
    }
    this->close();
    delete this;
}
