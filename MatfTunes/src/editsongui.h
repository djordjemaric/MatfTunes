#ifndef EDITSONGUI_H
#define EDITSONGUI_H

#include <QMainWindow>
#include "song.h"
#include "songitemui.h"

namespace Ui {
class EditSongUI;
}

class EditSongUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit EditSongUI(Song* song, SongItemUI* songItem, QWidget *parent = nullptr);
    ~EditSongUI();

private slots:
    void change_icon();
    void save_changes();
signals:
    void savedSongChanges(Song*);
private:
    Ui::EditSongUI *ui;
    Song* m_song;
    SongItemUI* m_songItem;
    QString  newPathToImg = "";

    void setupUI();
};

#endif // EDITSONGUI_H
