#ifndef IMPLICITPLAYLIST_H
#define IMPLICITPLAYLIST_H

#include "playlist.h"

class ImplicitPlaylist : public Playlist
{
public:
    ImplicitPlaylist();
    ImplicitPlaylist(QString name);
    void changeName(QString) override;
    void changeCover(QString) override;
};

#endif // IMPLICITPLAYLIST_H
