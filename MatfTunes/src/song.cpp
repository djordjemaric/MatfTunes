#include "song.h"
#include <QMediaMetaData>
#include <QTime>
#include <taglib/taglib.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>

Song::Song()
{

}

Song::Song(QString path) : m_path(path){
    TagLib::MPEG::File mpegFile(path.toUtf8().constData());
    TagLib::ID3v2::Tag *tag = mpegFile.ID3v2Tag();

    m_name = QString::fromStdString(tag->title().to8Bit(true));
    m_artist = QString::fromStdString(tag->artist().to8Bit(true));
    m_duration = mpegFile.audioProperties()->lengthInSeconds();
    m_path_to_img = ":/resources/default_artist_img.jpg";

    if(m_name == "")
        m_name = "Unknown";

    if(m_artist == "")
        m_artist = "Unknown";

    if (m_lyrics == "")
        m_lyrics = "Unknown";

    m_timesPlayed = 0;
    m_liked = false;
}

Song::~Song(){
}

void Song::setName(QString name){
    m_name = name;

    TagLib::MPEG::File mpegFile(m_path.toUtf8().constData());
    TagLib::ID3v2::Tag *tag = mpegFile.ID3v2Tag(true);

    tag->setTitle(name.toStdString());
    mpegFile.save();
}

void Song::setArtist(QString artist){
    m_artist = artist;

    TagLib::MPEG::File mpegFile(m_path.toUtf8().constData());
    TagLib::ID3v2::Tag *tag = mpegFile.ID3v2Tag(true);

    tag->setArtist(artist.toStdString());
    mpegFile.save();
}


void Song::setPathToImg(QString new_path_to_img)
{
    m_path_to_img = new_path_to_img;
}

void Song::setLyrics(QString lyrics) {
    m_lyrics = lyrics;
}

void Song::incrementTimesPlayed(){
    m_timesPlayed++;
}

void Song::like()
{
    m_liked = not m_liked;
}


QVariant Song::toVariant()
{
    QVariantMap map;
    map.insert("path", m_path);
    map.insert("artist", m_artist);
    map.insert("name", m_name);
    map.insert("duration", m_duration);
    map.insert("timesPlayed", m_timesPlayed);
    map.insert("liked", m_liked);
    map.insert("pathToImg", m_path_to_img);
    map.insert("lyrics", m_lyrics);

    return map;
}

void Song::fromVariant(const QVariant &variant)
{
    auto map = variant.toMap();
    m_path = map.value("path").toString();
    m_artist = map.value("artist").toString();
    m_name = map.value("name").toString();
    m_duration = map.value("duration").toUInt();
    m_timesPlayed = map.value("timesPlayed").toUInt();
    m_liked = map.value("liked").toBool();
    m_path_to_img = map.value("pathToImg").toString();
    m_lyrics = map.value("lyrics").toString();
}
