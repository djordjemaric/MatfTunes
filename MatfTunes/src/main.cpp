#include "settings.h"
#include "mainwindow.h"
#include <QApplication>
#include <QtMultimedia>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Settings::settings();
    MainWindow w;
    unsigned width = Settings::settings().width();
    unsigned height = Settings::settings().height();
    w.resize(width,height);

    w.show();
    return a.exec();
}
