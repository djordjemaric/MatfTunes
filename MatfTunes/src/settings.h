#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include "../serialization/jsonserializer.h"

class Settings : public Serializable
{
public:
    static Settings& settings();

    void setDefaultDir(QString&);
    void setHeight(unsigned);
    void setWidth(unsigned);

    inline QString defaultDir() const { return m_defaultDir; }
    inline unsigned height() const { return m_height; }
    inline unsigned width() const { return m_width; }
    JsonSerializer *serializer();
    // Serializable interface
    QVariant toVariant() final;
    void fromVariant(const QVariant &variant) final;
private:
    Settings();
    ~Settings();
    Settings(const Settings&) = delete;
    Settings operator=(const Settings&) = delete;


    QString m_defaultDir;
    unsigned m_width = 1000;
    unsigned m_height = 1000;
    JsonSerializer* m_serializer;
};

#endif // SETTINGS_H
