#include "songlibrarywindow.h"
#include "ui_songlibrarywindow.h"
#include "songitemui.h"
#include "songlibrary.h"
#include "playlist.h"


#include <QListWidgetItem>
#include <QVector>
#include <QListWidget>

SongLibraryWindow::SongLibraryWindow(Playlist* p, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SongLibraryWindow)
{
    m_playlist = p;
    ui->setupUi(this);
    update_song_list();
    connect(ui->list_separate_window, &QListWidget::doubleClicked, this, &SongLibraryWindow::choose_song);
    connect(ui->list_separate_window, &QListWidget::itemSelectionChanged, this, &SongLibraryWindow::select_songs);
    connect(ui->btn_add_songs, &QPushButton::clicked, this, &SongLibraryWindow::add_multiple_songs);

    this->setWindowTitle("SongLibrary");
    this->setStyleSheet("background-color: #2f303e; border: none;");
    ui->list_separate_window->setStyleSheet("QListWidget::item:hover { background-color: #b590ae; }"
                                            "QListWidget::item:selected {background-color: #785d7b;}");
}

SongLibraryWindow::~SongLibraryWindow()
{
    delete ui;
}


void SongLibraryWindow::display_song(Song* s){
    QListWidgetItem* item = new QListWidgetItem;
    SongItemUI* songItem = new SongItemUI(s, item, nullptr, nullptr, this);

    item->setSizeHint(songItem->sizeHint());
    ui->list_separate_window->addItem(item);
    ui->list_separate_window->setItemWidget(item,songItem);
}

void SongLibraryWindow::update_song_list() {
    ui->list_separate_window->clear();
    QVector<Song*> songs = SongLibrary::songLibrary().songs();
    for(Song* s : songs)
    {
        display_song(s);
    }
    //    ui->stacked_widget_pages->setCurrentIndex(2);
}


void SongLibraryWindow::choose_song()
{
    QListWidgetItem *chosen_item = ui->list_separate_window->currentItem();
    auto chosen_song = dynamic_cast<SongItemUI*>(ui->list_separate_window->itemWidget(chosen_item));
    Song *s = chosen_song->song();
    bool added = m_playlist->addSong(s);

    if (not added) {
        this->close();
        return;
    }

    emit songChosen(s, m_playlist);
    //TODO: check if this leaks memory
    this->close();
}

void SongLibraryWindow::select_songs()
{
    ui->list_separate_window->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

void SongLibraryWindow::add_multiple_songs()
{ 
    QList<QListWidgetItem*> selected_items = ui->list_separate_window->selectedItems();
    QVector<Song*> selected_songs;
    Song *current_song;

    for (QListWidgetItem* item : selected_items)
    {
        auto chosen_song = dynamic_cast<SongItemUI*>(ui->list_separate_window->itemWidget(item));
        current_song = chosen_song->song();
        if(m_playlist->addSong(current_song))
            selected_songs.append(current_song);
        else
            continue;
    }

    emit songs_chosen(selected_songs, m_playlist);
    //TODO: check if this leaks memory
    this->close();
}

