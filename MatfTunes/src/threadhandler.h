#ifndef THREADHANDLER_H
#define THREADHANDLER_H

#include <QListWidget>

#include "lyricssearchthread.h"
#include "songlibrary.h"

class ThreadHandler : public QObject
{
    Q_OBJECT
public:
    ThreadHandler();

    void startThreads(QString);
signals:
    void songFound(Song*);
private:
    void onThreadFinish(Song*);
};

#endif // THREADHANDLER_H
