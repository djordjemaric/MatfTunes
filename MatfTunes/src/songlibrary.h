#ifndef SONGLIBRARY_H
#define SONGLIBRARY_H

#include "song.h"
#include <QString>
#include <QVector>

class SongLibrary : public Serializable
{
public:
    static SongLibrary& songLibrary();

    void addSong(Song *song);
    Song* song(const QString& path);
    Song* addSongViaPath(QString& path);
    void removeSong(const Song *song);
    void removeAllSongs();
    inline QVector<Song *> songs() const { return m_songs; }
    QVector<Song*> songs(QString token);
    QVector<Song*> topNTracks(int n);

    //Serializable
    QVariant toVariant() final;
    void fromVariant(const QVariant &variant);

    void loadSongs();
    void saveSongs();

private:
    SongLibrary();
    SongLibrary(const SongLibrary&) = delete;
    SongLibrary operator=(const SongLibrary&) = delete;
    ~SongLibrary();

    QVector<Song*> m_songs;
};


#endif // SONGLIBRARY_H
