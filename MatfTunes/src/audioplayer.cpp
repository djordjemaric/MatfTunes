#include "audioplayer.h"
#include <math.h>

#include <QDebug>

AudioPlayer::AudioPlayer() : m_currentSong(nullptr) {
    m_audioOutput = new QAudioOutput(this);
    m_player = new QMediaPlayer(this);
    m_player->setAudioOutput(m_audioOutput);

    connect(m_player, &QMediaPlayer::positionChanged, this, &AudioPlayer::positionChanged);
    connect(m_player, &QMediaPlayer::mediaStatusChanged, this, &AudioPlayer::statusChanged);
    connect(this, &AudioPlayer::songFinished, this, &AudioPlayer::stopPlaying);
}

void AudioPlayer::setCurrentSong(Song *newCurrentSong)
{
    m_currentSong = newCurrentSong;
    if(m_currentSong == nullptr){
        m_player->stop();
        QUrl source = QUrl();
        m_player->setSource(source);
    }
}

AudioPlayer::~AudioPlayer() {
    stopPlaying();
    m_audioOutput->deleteLater();
    m_player->deleteLater();
}

AudioPlayer& AudioPlayer::player() {
    static AudioPlayer player {};
    return player;
}

Song *AudioPlayer::currentSong() const {
    return m_currentSong;
}

void AudioPlayer::setVolume(float volume) {
    if(volume <= -100){
        m_muted = false;
        invertMuted();
    }
    else {
        double naturalMax = 1000;
        double naturalMin = 0.1;

        double volumeLevel = pow(10, (volume / 1000.0) * log10(naturalMax / naturalMin)) * naturalMin;

        m_muted = true;
        invertMuted();
        m_audioOutput->setVolume(volumeLevel);
    }
}

void AudioPlayer::invertMuted() {
    m_muted = !m_muted;
    emit mutedChanged(m_muted);
    m_audioOutput->setMuted(m_muted);
}

void AudioPlayer::stopPlaying() {
    if (not playing())
        return;
    m_player->stop();
    m_currentSong = nullptr;
    emit songChanged(nullptr);
    QUrl source = QUrl();
    m_player->setSource(source);
}

void AudioPlayer::playSong(Song* s) {
    setCurrentSong(s);
    if(s != nullptr){
        s->incrementTimesPlayed();
        auto source = QUrl::fromLocalFile(s->path());
        m_player->setSource(source);
        emit songChanged(s);
        m_player->play();
    }
}

void AudioPlayer::play() {
    if (m_currentSong != nullptr && !(m_player->isPlaying()) ) {
        m_player->play();
    }
}

void AudioPlayer::pause() {
    if (m_currentSong != nullptr && m_player->isPlaying()) {
        m_player->pause();
    }
}

void AudioPlayer::setPosition(int64_t position) {
    if (m_currentSong != nullptr) {
        m_player->setPosition(position*1000);
    }
}

void AudioPlayer::positionChanged(int64_t position) {
    emit positionUpdated(position/1000);
}

void AudioPlayer::statusChanged(QMediaPlayer::MediaStatus status) {
    if (status == QMediaPlayer::EndOfMedia && m_player->loops() == QMediaPlayer::Once) {
        emit songFinished();
    }
}

void AudioPlayer::setLoop(bool loop) {
    if (loop) {
        m_player->setLoops(QMediaPlayer::Infinite);
    }
    else {
        m_player->setLoops(QMediaPlayer::Once);
    }
}
