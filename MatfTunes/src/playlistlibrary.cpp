#include "playlistlibrary.h"
#include <QString>
#include "implicitplaylist.h"
#include "../serialization/jsonserializer.h"


PlaylistLibrary& PlaylistLibrary::playlistLibrary(){
    static PlaylistLibrary playlistLibrary {};
    return playlistLibrary;
}

PlaylistLibrary::PlaylistLibrary() {
    loadPlaylists();
}

PlaylistLibrary::~PlaylistLibrary(){
    savePlaylists();
    for(Playlist* playlist : m_playlists){
        delete playlist;
    }
}

void PlaylistLibrary::addPlaylist(Playlist* playlist){
    m_playlists.append(playlist);
}

Playlist* PlaylistLibrary::addPlaylist(QString &name){
    if(name.isEmpty()){
        name = "Playlist ";
        int numOfPlaylists = numberOfPlaylists();
        name.append(QString::number(numOfPlaylists+1));
    }

    Playlist *playlist = new Playlist(name);
    addPlaylist(playlist);

    return playlist;
}

void PlaylistLibrary::removePlaylist(Playlist* playlist){
    for(int i = 0; i < numberOfPlaylists(); i++){
        if(m_playlists[i] == playlist){
            m_playlists.remove(i);
            delete playlist;
            playlist = nullptr;
            return;
        }
    }
}

QVector<Playlist*> PlaylistLibrary::playlists(QString token){
    QVector<Playlist*> playlistsToShow;

    for (Playlist* playlist : m_playlists) {
        if (playlist->name().toLower().contains(token.toLower())) {
            playlistsToShow.append(playlist);
        }
    }

    return playlistsToShow;
}

Playlist *PlaylistLibrary::playlistByName(QString name)
{
    auto playlists = PlaylistLibrary::playlistLibrary().playlists();
    for (auto el : playlists) {
        if (el->name() == name) {
            return el;
        }
    }
    return nullptr;
}

QVariant PlaylistLibrary::toVariant(){
    QVariantList list;

    for(Playlist* playlist : m_playlists){
        QVariant list_p = playlist->toVariant();
        list.append(list_p);
    }

    return list;
}

void PlaylistLibrary::fromVariant(const QVariant& variant){
    QVariantList list = variant.toList();

    if (list.size() >= 2) {
        m_playlists[0]->fromVariant(list[0].toList());
        m_playlists[1]->fromVariant(list[1].toList());
    }

    int n = list.size();
    for(int i = 2; i<n; i++) {
        QVariantList list_p = list[i].toList();
        Playlist* playlist = new Playlist();
        playlist->fromVariant(list_p);
        m_playlists.append(playlist);
    }
}

void PlaylistLibrary::savePlaylists(){
    JsonSerializer* js = new JsonSerializer();
    js->save(*this, "saved_playlists.json");
    delete js;
}

void PlaylistLibrary::loadPlaylists(){
    m_playlists.clear();
    ImplicitPlaylist* all = new ImplicitPlaylist("All songs");
    ImplicitPlaylist* liked = new ImplicitPlaylist("Liked Songs");
    m_playlists.append(all);
    m_playlists.append(liked);

    JsonSerializer* js = new JsonSerializer();
    js->load(*this, "saved_playlists.json");
    delete js;
}
