#ifndef LYRICSSEARCHTHREAD_H
#define LYRICSSEARCHTHREAD_H

#include <QMutex>
#include <QThread>
#include "song.h"

class LyricsSearchThread : public QThread
{
    Q_OBJECT
public:
    explicit LyricsSearchThread(Song* song, QString lyrics, QObject *parent = nullptr);

    void run() override;
signals:
    void finishedSearch(Song *);
private:
    QVector<int> zet(QString&);

    Song* m_song;
    QString m_lyrics;
};

#endif // LYRICSSEARCHTHREAD_H
