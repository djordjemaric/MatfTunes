#ifndef PLAYERITEMUI_H
#define PLAYERITEMUI_H

#include <QWidget>
#include "song.h"

namespace Ui {
class PlayerItemUI;
}

class PlayerItemUI : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerItemUI(QWidget *parent = nullptr);
    ~PlayerItemUI();


public slots:
    void closePlayer();
    void full_screen_player();
    void like_song();
    void play_pause();
    void shuffle();
    void next();
    void previous();
    void loops();
    void changeAudioIcon(bool);
    void songDeleted(Song*);
    void updateSongDetails(Song*);

signals:
    void close_player_signal();
    void full_screen_signal(bool);
    void liked_song_signal(Song*);
    void setMuted();
    void nextSong();
    void previousSong();
    void toggleShuffle();
    void loopChanged(bool);
    void positionChanged(int);
    void songChanged(Song*);

private slots:
    void updatePosition(int);
    void muteClicked();
    void updateMovedPosition(int);
    void updateData(Song* s);


private:
    Ui::PlayerItemUI *ui;
    Song* m_current_song;
    bool m_playing = 0;
    bool m_full_screen = 0;
    bool m_loop = 0;
    bool m_shuffle = 0;
    void setSongDetails(Song*);
    void setupIcons();
    void connectButtons();
    void connectSlots();
};

#endif // PLAYERITEMUI_H
