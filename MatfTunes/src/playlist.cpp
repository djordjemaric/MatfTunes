#include "playlist.h"
#include "songlibrary.h"
#include <QString>
#include <QRandomGenerator>

Playlist::Playlist(){
    m_currentIndex = 0;
    m_name = "New Playlist";
}

Playlist::Playlist(QString name) : m_name(name){
    m_currentIndex = 0;
}

Playlist::~Playlist(){
}

Song *Playlist::currentSong() const{
    if (m_songs.empty()) {
        throw std::out_of_range("Playlist is empty!");
    } else {
        return m_songs[m_currentIndex];
    }
}

bool Playlist::addSong(Song *s){
    if (m_songs.contains(s)) {
        return false;
    }

    m_songs.append(s);
    return true;
}

void Playlist::removeSong(Song *s) {
    for (int i = 0; i<m_songs.size(); i++) {
        if (m_songs[i] == s) {
            m_songs.remove(i);
            return;
        }
    }
}

void Playlist::setCurrentIndex(int i) {
    if (i >= 0 && i < numOfSongs())
        m_currentIndex = i;
}

void Playlist::changeName(QString name) {
    m_name = name;
}

void Playlist::changeCover(QString path) {
    m_cover_path = path;
}

Song* Playlist::next(){
    int numberOfSongs = numOfSongs();
    if (numberOfSongs > 0) {
        m_currentIndex = (m_currentIndex + 1 + numberOfSongs) % numberOfSongs;
        return m_songs[m_currentIndex];
    }
    return nullptr;
}

Song* Playlist::previous(){
    int numberOfSongs = numOfSongs();

    if (numberOfSongs > 0) {
        m_currentIndex = (m_currentIndex - 1 + numberOfSongs) % numberOfSongs;
        return m_songs[m_currentIndex];
    }
    return nullptr;
}

Song* Playlist::shuffle(){
    int numberOfSongs = numOfSongs();
    int index = QRandomGenerator::global()->bounded(numberOfSongs);
    if (numberOfSongs > 0) {

        int newIndex = (m_currentIndex + index + numberOfSongs) % numberOfSongs;
        //To avoid playing the same song
        if (m_currentIndex == newIndex)
            m_currentIndex = (m_currentIndex + 1 + numberOfSongs) % numberOfSongs;
        else
            m_currentIndex = newIndex;
        return m_songs[m_currentIndex];
    }
    return nullptr;
}

//Serializable
QVariant Playlist::toVariant(){
    QVariantList list;
    QVariantMap map;
    map.insert("name",m_name);
    map.insert("playlistCover", m_cover_path);

    list.append(map);
    for(Song* song : m_songs){
        QString path = song->path();
        QVariantMap map;
        map.insert("path",path);
        list.append(map);
    }

    return list;
}

void Playlist::fromVariant(const QVariant &variant){
    QList<QVariant> list = variant.toList();
    m_songs.clear();
    QVariantMap map = list[0].toMap();
    m_name = map.value("name").toString();
    m_cover_path = map.value("playlistCover").toString();

    int listLength = list.length();

    for(int i = 0;i < listLength; i++){
        QVariantMap map = list[i].toMap();
        QString path = map.value("path").toString();
        Song* song = SongLibrary::songLibrary().song(path);

        if(song != nullptr){
            addSong(song);
        }
    }
}
